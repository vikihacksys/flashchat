//
//  User.m
//  FlashChat
//
//  Created by Vivek Radadiya on 23/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "User.h"
#import <Realm/Realm.h>

@implementation User

+ (NSArray<NSString *> *) requiredProperties {
    return @[@"userID", @"username", @"email", @"password"];
}

+ (NSString *) primaryKey {
    return @"userID";
}

+ (NSDictionary *) defaultPropertyValues {
    return @{@"isLoggedIn" : @NO};
}

//- (id) initWithCoder: (nonnull NSCoder *) decoder {
//
//    if (self = [super init]) {
//
//        self.userProfilePic = [decoder decodeObjectForKey: @"userProfilePic"];
//        self.username = [decoder decodeObjectForKey: @"username"];
//        self.email = [decoder decodeObjectForKey: @"email"];
//        self.password = [decoder decodeObjectForKey: @"password"];
//        self.about = [decoder decodeObjectForKey: @"about"];
//
//    }
//
//    return self;
//
//}
//
//- (void) encodeWithCoder: (nonnull NSCoder *) encoder {
//
//    [encoder encodeObject: self.userProfilePic forKey: @"userProfilePic"];
//    [encoder encodeObject: self.username forKey: @"username"];
//    [encoder encodeObject: self.email forKey: @"email"];
//    [encoder encodeObject: self.password forKey: @"password"];
//    [encoder encodeObject: self.about forKey: @"about"];
//
//}


@end
