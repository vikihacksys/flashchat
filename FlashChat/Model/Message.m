//
//  Message.m
//  FlashChat
//
//  Created by Vivek Radadiya on 27/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "Message.h"
#import "PrefixHeader.pch"
#import <Realm/Realm.h>

@implementation Message

+ (NSArray<NSString *> *) requiredProperties {
    return @[@"messageID", @"message", @"messageType", @"messageAttachment", @"dateTime", @"senderID", @"receiverID"];
}

+ (NSString *) primaryKey {
    return @"messageID";
}

@end
