//
//  Message.h
//  FlashChat
//
//  Created by Vivek Radadiya on 27/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrefixHeader.pch"
#import <Realm/Realm.h>

NS_ASSUME_NONNULL_BEGIN

@interface Message : RLMObject

@property NSString *messageID;
@property NSString *message;
@property NSString *messageAttachment;
@property NSString *messageType;
@property NSString *dateTime;
@property NSString *senderID;
@property NSString *receiverID;

@end

NS_ASSUME_NONNULL_END
