//
//  User.h
//  FlashChat
//
//  Created by Vivek Radadiya on 23/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrefixHeader.pch"
#import <Realm/Realm.h>

NS_ASSUME_NONNULL_BEGIN

@interface User : RLMObject

@property NSString *userID;
@property NSString *userProfilePic;
@property NSString *username;
@property NSString *email;
@property NSString *password;
@property NSString *aboutMe;
@property BOOL isLoggedIn;

@end

NS_ASSUME_NONNULL_END
