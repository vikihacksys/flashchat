//
//  Helpers.h
//  FlashChat
//
//  Created by Vivek Radadiya on 19/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrefixHeader.pch"

NS_ASSUME_NONNULL_BEGIN

@interface Helpers : NSObject


+ (UIActivityIndicatorView *) createActivityIndicator;

+ (UIRefreshControl *) createRefreshControlWithTintColor: (UIColor * _Nullable) color Message: (NSString *) title Attributes: (NSDictionary<NSAttributedStringKey, id> * _Nullable) attributes;

+ (BOOL) isValidEmail: (NSString *) emailString;

+ (NSDate *) dateFromString: (NSString *) date;

@end

NS_ASSUME_NONNULL_END
