//
//  Helpers.m
//  FlashChat
//
//  Created by Vivek Radadiya on 19/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "Helpers.h"
#import "PrefixHeader.pch"

@implementation Helpers

+ (UIActivityIndicatorView *) createActivityIndicator {
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleLarge];
    
    [activityIndicator setAlpha: 1.0];
    [activityIndicator setCenter: CGPointMake(([UIScreen mainScreen].bounds.size.width) / 2, ([UIScreen mainScreen].bounds.size.height) / 2)];
    
    return activityIndicator;
    
}

+ (UIRefreshControl *) createRefreshControlWithTintColor: (UIColor * _Nullable) color Message: (NSString *) title Attributes: (NSDictionary<NSAttributedStringKey, id> * _Nullable) attributes {
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    
    if (color) {
        [refreshControl setTintColor: color];
    }
    
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString: title attributes: attributes];
    [refreshControl setAttributedTitle: attString];
    
    return refreshControl;
    
}

+ (BOOL) isValidEmail: (NSString *) emailString {
    
     NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
     NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
     NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
     if (regExMatches == 0) {
         return NO;
     }
     else
         return YES;
    
}

+ (NSDate *) dateFromString: (NSString *) date {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"yyyy-MM-dd"];
    NSArray *dateComponents = [[NSArray alloc] initWithArray: [date componentsSeparatedByString: @" "]];
    NSDate *convertedDate = [dateFormatter dateFromString: [dateComponents firstObject]];
    return convertedDate;
    
}




@end
