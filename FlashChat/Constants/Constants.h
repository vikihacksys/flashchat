//
//  Constants.h
//  FlashChat
//
//  Created by Vivek Radadiya on 18/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef Constants_h
#define Constants_h

#define cStatusBarBackground [UIColor colorWithRed: 10.0/255.0 green: 61.0/255.0 blue: 98.0/255.0 alpha: 1.0]

#define kUserProfilePlaceholderImage [UIImage systemImageNamed: @"person.circle"]

#endif /* Constants_h */


//MARK:- Application Constants

static NSString *kAppName = @"FlashChat";
static NSString *kStoryBoardName = @"Main";


//MARK:- UIViewController's Storyboard-ID Constants

static NSString *kBaseVC = @"ViewController";
static NSString *kRegisterVC = @"RegisterViewController";
static NSString *kLoginVC = @"LoginViewController";
static NSString *kTabBarVC = @"TabBarViewController";
static NSString *kChatListVC = @"ChatListViewController";
static NSString *kChatVC = @"ChatViewController";
static NSString *kProfileVC = @"ProfileViewController";
static NSString *kEditProfileVC = @"EditProfileViewController";
static NSString *kDisplayImageVC = @"DisplayImageViewController";
static NSString *kLocationVC = @"LocationViewController";


//MARK:- Cell Identifier Constants

static NSString *kChatListCellIdentifier = @"ChatListTableViewCell";
static NSString *kSenderMessageCellIdentifier = @"SenderMessageTableViewCell";
static NSString *kReceiverMessageCellIdentifier = @"ReceiverMessageTableViewCell";
static NSString *kSenderAttachmentCellIdentifier = @"SenderAttachmentTableViewCell";
static NSString *kReceiverAttachmentCellIdentifier = @"ReceiverAttachmentTableViewCell";


//MARK:- Firebase Constants

static NSString *kUsersCollection = @"Users";
static NSString *kChatsCollection = @"Chats";
static NSString *kMessagesSubCollection = @"Messages";
static NSString *kFBStorageImageRefPathName = @"images";
static NSString *kFBStorageChatRefPathName = @"chats";
static NSString *kFBStorageProfileName = @"profile.jpg";

static NSString *kUserIDField = @"userID";
static NSString *kUserProfilePicField = @"userProfilePic";
static NSString *kUsernameField = @"username";
static NSString *kEmailField = @"email";
static NSString *kPasswordField = @"password";
static NSString *kAboutMeField = @"aboutMe";
static NSString *kIsLoggedInField = @"isLoggedIn";

static NSString *kMessageIDField = @"messageID";
static NSString *kMessageField = @"message";
static NSString *kMessageAttachmentField = @"messageAttachment";
static NSString *kMessageTypeField = @"messageType";
static NSString *kDateTimeField = @"dateTime";
static NSString *kSenderIDField = @"senderID";
static NSString *kReceiverIDField = @"receiverID";

static NSString *kMessageTypeText = @"Text";
static NSString *kMessageTypeImage = @"Image";
static NSString *kMessageTypeLocation = @"Location";


//MARK:- NSNotificationCenter Constants

static NSString *kSetupRootVCNotification = @"RootViewControllerSetup";


//MARK:- Google AdMob Constants

static NSString *kGoogleAdMobAppID = @"ca-app-pub-3940256099942544~1458002511";
static NSString *kGoogleAdMobBannerAppUnitID = @"ca-app-pub-3940256099942544/2934735716";
static NSString *kGoogleAdMobInterstitialAppUnitID = @"ca-app-pub-3940256099942544/4411468910";


