//
//  ChatViewController.h
//  FlashChat
//
//  Created by Vivek Radadiya on 27/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <UIKit/UIKit.h>
@class User;

NS_ASSUME_NONNULL_BEGIN

@interface ChatViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *receiverUsernameLabel;
@property (weak, nonatomic) IBOutlet UITableView *messageTableView;
@property (weak, nonatomic) IBOutlet UIView *attachmentView;
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightForBottomView;
@property (weak, nonatomic) IBOutlet UIButton *btnShareLocationButton;
@property (weak, nonatomic) IBOutlet UIButton *btnShareImageButton;
@property (weak, nonatomic) IBOutlet UILabel *noChatsAvailableLabel;

- (IBAction)backButtonTapped:(UIButton *)sender;
- (IBAction)profileButtonTapped:(UIButton *)sender;
- (IBAction)shareLocationButtonTapped:(UIButton *)sender;
- (IBAction)shareImageButtonTapped:(UIButton *)sender;
- (IBAction)attachmentButtonTapped:(UIButton *)sender;
- (IBAction)sendMessageButtonTapped:(UIButton *)sender;

@property User *receiverUser;

@end

NS_ASSUME_NONNULL_END
