//
//  ChatListViewController.h
//  FlashChat
//
//  Created by Vivek Radadiya on 22/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatListViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *noUsersLabel;
@property (weak, nonatomic) IBOutlet UITableView *tblChatListTableView;
@property (weak, nonatomic) IBOutlet GADBannerView *gadBannerView;

@end

NS_ASSUME_NONNULL_END
