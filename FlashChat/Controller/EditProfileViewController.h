//
//  EditProfileViewController.h
//  FlashChat
//
//  Created by Vivek Radadiya on 22/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EditProfileViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *userProfileImageView;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextView *aboutMeTextField;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdateProfileButton;

- (IBAction)backButtonTapped:(UIButton *)sender;
- (IBAction)updateProfileButtonTapped:(UIButton *)sender;


@end

NS_ASSUME_NONNULL_END
