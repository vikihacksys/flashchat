//
//  ProfileViewController.m
//  FlashChat
//
//  Created by Vivek Radadiya on 22/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "ProfileViewController.h"
#import "PrefixHeader.pch"

@interface ProfileViewController ()
{
    RLMResults<User *> *results;
    User *user;
    RLMRealm *realm;
}
@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupLayout];
    
}
 
- (void) viewWillAppear: (BOOL) animated {
    
    if (_receiverUser != nil) {
        
        [_btnBackButton setHidden: NO];
        [_btnProfileEditButton setHidden: YES];
        [_btnLogoutButton setHidden: YES];
        _userProfileImageView.sd_imageIndicator = SDWebImageActivityIndicator.grayLargeIndicator;
        [_userProfileImageView sd_setImageWithURL: [NSURL URLWithString: _receiverUser.userProfilePic] placeholderImage: kUserProfilePlaceholderImage];
        [_usernameLabel setText: _receiverUser.username];
        [_emailLabel setText: _receiverUser.email];
        [_aboutMeLabel setText: _receiverUser.aboutMe];
        
    } else {
        
        results = [User allObjects];
        user = [results firstObject];
        [_btnBackButton setHidden: YES];
        [_btnProfileEditButton setHidden: NO];
        [_btnLogoutButton setHidden: NO];
        _userProfileImageView.sd_imageIndicator = SDWebImageActivityIndicator.grayLargeIndicator;
        [_userProfileImageView sd_setImageWithURL: [NSURL URLWithString: user.userProfilePic] placeholderImage: kUserProfilePlaceholderImage];
        [_usernameLabel setText: user.username];
        [_emailLabel setText: user.email];
        [_aboutMeLabel setText: user.aboutMe];
        
    }
    
}


//MARK:- UIButton's action methods

- (IBAction)logoutButtonTapped:(UIButton *)sender {
    NSError *error;
    [[FIRAuth auth] signOut: &error];
    if (error) {
        [self.view makeToast: error.localizedDescription];
    } else {
        
        [realm beginWriteTransaction];
        [user setIsLoggedIn: NO];
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName: kSetupRootVCNotification object: nil];
        
        [realm beginWriteTransaction];
        [realm deleteAllObjects];
        [realm commitWriteTransaction];
        
    }
    
}

- (IBAction)profileButtonTapped:(UIButton *)sender {
    EditProfileViewController *editProfileVC = [self.storyboard instantiateViewControllerWithIdentifier: kEditProfileVC];
    [editProfileVC setHidesBottomBarWhenPushed: YES];
    [self.navigationController pushViewController: editProfileVC animated: YES];
}

- (IBAction)backButtonTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated: YES];
}


//MARK:- User-defined methods

- (void) setStatusBarBackgroundColour {
    
    UIWindow *windowRoot = nil;
    NSArray *windows = [[UIApplication sharedApplication] windows];
    for (UIWindow *window in windows) {
        if (window.isKeyWindow) {
            windowRoot = window;
            break;
        }
    }
    
    UIView *statusBar = [[UIView alloc] initWithFrame: windowRoot.windowScene.statusBarManager.statusBarFrame];
    [statusBar setBackgroundColor: cStatusBarBackground];
    [self.view addSubview: statusBar];
    
}

- (void) setupLayout {
    
    [self setStatusBarBackgroundColour];
    realm = [RLMRealm defaultRealm];
    [[_btnLogoutButton layer] setCornerRadius: (_btnLogoutButton.frame.size.height / 2.0)];
    [[_userProfileImageView layer] setCornerRadius: (_userProfileImageView.frame.size.height / 2.0)];
    
}


@end
