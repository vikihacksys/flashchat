//
//  SenderAttachmentTableViewCell.m
//  FlashChat
//
//  Created by Vivek Radadiya on 28/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "SenderAttachmentTableViewCell.h"

@implementation SenderAttachmentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
