//
//  LoginViewController.m
//  FlashChat
//
//  Created by Vivek Radadiya on 22/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "LoginViewController.h"
#import "PrefixHeader.pch"

@interface LoginViewController ()
{
    
    UIActivityIndicatorView *activityIndicator;
    RLMRealm *realm;
    
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupLayout];
    
}


//MARK:- UIButton's action methods

- (IBAction)loginButtonTapped:(UIButton *)sender {
    
    if ([self validateUserRegistration]) {
        
        NSString *strEmail = _emailTextField.text;
        NSString *strPassword = _passwordTextField.text;
        
        [activityIndicator startAnimating];
        
        [FIRAuth.auth signInWithEmail: strEmail password: strPassword completion:^(FIRAuthDataResult * _Nullable authResult, NSError * _Nullable error) {
            
            if (error) {
                [self.view makeToast: error.localizedDescription];
            } else {
                
                FIRCollectionReference *collectionRef = [[FIRFirestore firestore] collectionWithPath: kUsersCollection];
                [collectionRef getDocumentsWithCompletion:^(FIRQuerySnapshot * _Nullable snapshot, NSError * _Nullable error) {
                   
                    if (error) {
                        [self.view makeToast: @"Error while login."];
                    } else {
                        
                        for (FIRDocumentSnapshot *document in snapshot.documents) {
                            
                            NSDictionary *dicData = [[NSDictionary alloc] initWithDictionary: document.data];
                            if ([[dicData valueForKey: kEmailField] isEqualToString: strEmail]) {
                                
                                User *user = [[User alloc] init];
                                [user setUserID: [dicData valueForKey: kUserIDField]];
                                [user setUserProfilePic: [dicData valueForKey: kUserProfilePicField]];
                                [user setUsername: [dicData valueForKey: kUsernameField]];
                                [user setEmail: [dicData valueForKey: kEmailField]];
                                [user setPassword: [dicData valueForKey: kPasswordField]];
                                [user setAboutMe: [dicData valueForKey: kAboutMeField]];
                                [user setIsLoggedIn: YES];
                                
                                [self->realm beginWriteTransaction];
                                [self->realm addOrUpdateObject: user];
                                [self->realm commitWriteTransaction];
                                
                                [self.view makeToast: @"Login successfully."];
                                
                                [[NSNotificationCenter defaultCenter] postNotificationName: kSetupRootVCNotification object: nil];
                                
                            }
                            
                        }
                        
                    }
                    [self->activityIndicator stopAnimating];
                    
                }];
                
            }
            
            [self->activityIndicator stopAnimating];
            
        }];
        
    }
    
}

- (IBAction)backButtonTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated: YES];
}


//MARK:- User-defined methods

- (void) setStatusBarBackgroundColour {
    
    UIWindow *windowRoot = nil;
    NSArray *windows = [[UIApplication sharedApplication] windows];
    for (UIWindow *window in windows) {
        if (window.isKeyWindow) {
            windowRoot = window;
            break;
        }
    }
    
    UIView *statusBar = [[UIView alloc] initWithFrame: windowRoot.windowScene.statusBarManager.statusBarFrame];
    [statusBar setBackgroundColor: cStatusBarBackground];
    [self.view addSubview: statusBar];
    
}

- (void) setupLayout {
    
    [self setStatusBarBackgroundColour];
    realm = [RLMRealm defaultRealm];
    [[_loginButton layer] setCornerRadius: (_loginButton.frame.size.height / 2.0)];
    activityIndicator = [Helpers createActivityIndicator];
    [self.view addSubview: activityIndicator];
    
}

- (BOOL) validateUserRegistration {
    
    if ([_emailTextField.text isEqualToString: @""]) {
        [self.view makeToast: @"Please enter your email."];
        return NO;
    } else if (![Helpers isValidEmail: _emailTextField.text]) {
        [self.view makeToast: @"Please enter valid email."];
        return NO;
    } else if ([_passwordTextField.text isEqualToString: @""]) {
        [self.view makeToast: @"Please enter your password."];
        return NO;
    } else {
        return YES;
    }
    
}


@end
