//
//  LoginViewController.h
//  FlashChat
//
//  Created by Vivek Radadiya on 22/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginViewController : UIViewController


@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

- (IBAction)backButtonTapped:(UIButton *)sender;
- (IBAction)loginButtonTapped:(UIButton *)sender;


@end

NS_ASSUME_NONNULL_END
