//
//  RegisterViewController.m
//  FlashChat
//
//  Created by Vivek Radadiya on 18/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "RegisterViewController.h"
#import "PrefixHeader.pch"
#import <Realm/Realm.h>

@interface RegisterViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    
    UIActivityIndicatorView *activityIndicator;
    UIImagePickerController *imagePickerController;
    UIImage *currentUserProfilePic;
    AppDelegate *appDelegate;
    RLMRealm *realm;
    NSString *strUserProfileImageURL;
    
}
@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupLayout];
    
}


//MARK:- UIButton's action methods

- (IBAction)userProfileButtonTapped:(UIButton *)sender {
    
    imagePickerController = [[UIImagePickerController alloc] init];
    [imagePickerController setDelegate: self];
    [imagePickerController setAllowsEditing: YES];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle: @"Select an image" message: @"" preferredStyle: UIAlertControllerStyleActionSheet];
    
    UIAlertAction *galleryAction = [UIAlertAction actionWithTitle: @"From Gallery" style: UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self->imagePickerController setSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
        [self presentViewController: self->imagePickerController animated: YES completion: nil];
        
    }];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle: @"From Camera" style: UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self->imagePickerController setSourceType: UIImagePickerControllerSourceTypeCamera];
        [self presentViewController: self->imagePickerController animated: YES completion: nil];
        
    }];
    
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle: @"Delete image" style: UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        self->currentUserProfilePic = kUserProfilePlaceholderImage;
        [self->_imgUserProfileImageView setImage: self->currentUserProfilePic];
        
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle: @"Cancel" style: UIAlertActionStyleCancel handler: nil];
    
    [alertController addAction: galleryAction];
    [alertController addAction: cameraAction];
    [alertController addAction: deleteAction];
    [alertController addAction: cancelAction];
    
    [self presentViewController: alertController animated: YES completion: nil];
    
}

- (IBAction)registerButtonTapped:(UIButton *)sender {
    
    if ([self validateUserRegistration]) {
        
        NSString *strEmail = _emailTextField.text;
        NSString *strPassword = _passwordTextField.text;
        
        [activityIndicator startAnimating];
        
        [FIRAuth.auth createUserWithEmail: strEmail password: strPassword completion:^(FIRAuthDataResult * _Nullable authResult, NSError * _Nullable error) {
            
            if (error) {
                [self.view makeToast: error.localizedDescription];
                [self->activityIndicator stopAnimating];
            } else {
                
                if (self->currentUserProfilePic != kUserProfilePlaceholderImage) {
                    
                    NSString *userPath = [[strEmail componentsSeparatedByString: @"@"] firstObject];
                    FIRStorageReference *profileRef = [[FirebaseManager userProfileImageRef] child: [NSString stringWithFormat: @"%@/%@", userPath, kFBStorageProfileName]];
                    FIRStorageMetadata *metadata = [[FIRStorageMetadata alloc] init];
                    metadata.contentType = @"image/jpeg";
                    
                    [profileRef putData: UIImageJPEGRepresentation(self->currentUserProfilePic, 1.0) metadata: metadata completion:^(FIRStorageMetadata * _Nullable metadata, NSError * _Nullable error) {
                        
                        if (error) {
                            //                        [self.view makeToast: error.localizedDescription];
                            [self deleteFBAuthUserAccount];
                        } else {
                            
                            [profileRef downloadURLWithCompletion:^(NSURL * _Nullable URL, NSError * _Nullable error) {
                                
                                if (error) {
                                    
                                    //                                [self.view makeToast: error.localizedDescription];
                                    [profileRef deleteWithCompletion:^(NSError * _Nullable error) {
                                        if (error) {
                                            //                                        [self.view makeToast: error.localizedDescription];
                                        }
                                        [self deleteFBAuthUserAccount];
                                    }];
                                    
                                } else {
                                    
                                    self->strUserProfileImageURL = [URL absoluteString];
                                    [self saveUserDataToFBFirestore];
                                    
                                }
                                
                            }];
                            
                        }
                        
                    }];
                    
                } else {
                    [self saveUserDataToFBFirestore];
                }
                
            }
            
        }];
        
    }
    
}

- (IBAction)backButtonTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated: YES];
}


//MARK:- UIImagePickerControllerDelegate's methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info {
    
    UIImage *pickedImage = info[UIImagePickerControllerEditedImage];
    
    currentUserProfilePic = pickedImage;
    [_imgUserProfileImageView setImage: pickedImage];
    [self dismissViewControllerAnimated: YES completion: nil];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated: YES completion: nil];
}


//MARK:- User-defined methods

- (void) setStatusBarBackgroundColour {
    
    UIWindow *windowRoot = nil;
    NSArray *windows = [[UIApplication sharedApplication] windows];
    for (UIWindow *window in windows) {
        if (window.isKeyWindow) {
            windowRoot = window;
            break;
        }
    }
    
    UIView *statusBar = [[UIView alloc] initWithFrame: windowRoot.windowScene.statusBarManager.statusBarFrame];
    [statusBar setBackgroundColor: cStatusBarBackground];
    [self.view addSubview: statusBar];
    
}

- (void) setupLayout {
    
    [self setStatusBarBackgroundColour];
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    realm = [RLMRealm defaultRealm];
    currentUserProfilePic = kUserProfilePlaceholderImage;
    strUserProfileImageURL = @"";
    [_imgUserProfileImageView setImage: currentUserProfilePic];
    [[_imgUserProfileImageView layer] setCornerRadius: (_imgUserProfileImageView.frame.size.height / 2.0)];
    [[_btnRegister layer] setCornerRadius: (_btnRegister.frame.size.height / 2.0)];
    activityIndicator = [Helpers createActivityIndicator];
    [self.view addSubview: activityIndicator];
    
}

- (BOOL) validateUserRegistration {
    
    if ([_usernameTextField.text isEqualToString: @""]) {
        [self.view makeToast: @"Please enter your username."];
        return NO;
    } else if ([_emailTextField.text isEqualToString: @""]) {
        [self.view makeToast: @"Please enter your email."];
        return NO;
    } else if (![Helpers isValidEmail: _emailTextField.text]) {
        [self.view makeToast: @"Please enter valid email."];
        return NO;
    } else if ([_passwordTextField.text isEqualToString: @""]) {
        [self.view makeToast: @"Please enter your password."];
        return NO;
    } else {
        return YES;
    }
    
}

- (void) deleteFBAuthUserAccount {
    FIRUser *currentUser = [[FIRAuth auth] currentUser];
    [currentUser deleteWithCompletion:^(NSError * _Nullable error) {
        if (error) {
            [self.view makeToast: error.localizedDescription];
        } else {
            [self.view makeToast: @"Error occured while registering your account."];
        }
        [self->activityIndicator stopAnimating];
    }];
}

- (void) saveUserDataToFBFirestore {
    
    NSString *strEmail = _emailTextField.text;
    NSString *strPassword = _passwordTextField.text;
    NSString *strUsername = self->_usernameTextField.text;
    NSString *strAboutMe = self->_aboutMeTextView.text;
    
    FIRCollectionReference *collectionRef = [[FIRFirestore firestore] collectionWithPath: kUsersCollection];
    FIRDocumentReference *documentRef = [collectionRef documentWithAutoID];
    NSString *strDocumentID = documentRef.documentID;
    [documentRef setData: @{
        
        kUserIDField: strDocumentID,
        kUserProfilePicField: strUserProfileImageURL,
        kUsernameField: strUsername,
        kEmailField: strEmail,
        kPasswordField: strPassword,
        kAboutMeField: strAboutMe
        
    } completion:^(NSError * _Nullable error) {
        
        if (error != nil) {
            //                                        [self.view makeToast: error.localizedDescription];
            [self deleteFBAuthUserAccount];
        } else {
            
            User *user = [[User alloc] init];
            [user setUserID: strDocumentID];
            [user setUserProfilePic: self->strUserProfileImageURL];
            [user setUsername: strUsername];
            [user setEmail: strEmail];
            [user setPassword: strPassword];
            [user setAboutMe: strAboutMe];
            [user setIsLoggedIn: YES];
            
            [self->realm beginWriteTransaction];
            [self->realm addObject: user];
            [self->realm commitWriteTransaction];
            
            [self.view makeToast: @"Registered successfully."];
            [[NSNotificationCenter defaultCenter] postNotificationName: kSetupRootVCNotification object: nil];
            
        }
        
        [self->activityIndicator stopAnimating];
        
    }];
    
}

@end
