//
//  ChatListViewController.m
//  FlashChat
//
//  Created by Vivek Radadiya on 22/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "ChatListViewController.h"
#import "PrefixHeader.pch"

@interface ChatListViewController () <UITableViewDelegate, UITableViewDataSource, GADBannerViewDelegate, GADInterstitialDelegate>
{
    
    UIActivityIndicatorView *activityIndicator;
    UIRefreshControl *refreshControl;
    NSMutableArray *arrUsers;
    RLMResults<User *> *results;
    User *user;
    GADInterstitial *interstitialAdView;
    
}
@end

@implementation ChatListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupLayout];
    
}

- (void) viewWillAppear: (BOOL) animated {
    [super viewWillAppear: animated];
    
    [self fetchAllUsersFromFBFirestore];
    
}


//MARK:- UITableView's delegate and datasource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrUsers.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 57.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ChatListTableViewCell *chatListCell = (ChatListTableViewCell *) [tableView dequeueReusableCellWithIdentifier: kChatListCellIdentifier forIndexPath: indexPath];
    
    User *userData = [arrUsers objectAtIndex: indexPath.row];
    
    [[chatListCell.imgUserProfileImage layer] setCornerRadius: (chatListCell.imgUserProfileImage.frame.size.height / 2.0)];
    chatListCell.imgUserProfileImage.sd_imageIndicator = SDWebImageActivityIndicator.grayIndicator;
    [chatListCell.imgUserProfileImage sd_setImageWithURL: [NSURL URLWithString: userData.userProfilePic] placeholderImage: kUserProfilePlaceholderImage];
    [chatListCell.lblUsernameLabel setText: userData.username];
    [chatListCell.lblLastMessageLabel setText: userData.aboutMe];
    
    return chatListCell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (interstitialAdView.isReady) {
        int randomNum = arc4random_uniform(3);
        if (randomNum == 2) {
            [interstitialAdView presentFromRootViewController: self];
        }
    }
    ChatViewController *chatVC = [self.storyboard instantiateViewControllerWithIdentifier: kChatVC];
    [chatVC setHidesBottomBarWhenPushed: YES];
    chatVC.receiverUser = [arrUsers objectAtIndex: indexPath.row];
    [self.navigationController pushViewController: chatVC animated: YES];
}


//MARK:- GADBannerViewDelegate methods

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    NSLog(@"Ad received");
    [_gadBannerView setHidden: NO];
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"Error receiving ad : %@", error.localizedDescription);
    [_gadBannerView setHidden: YES];
}

- (void) interstitialDidDismissScreen: (GADInterstitial *) ad {
    interstitialAdView = [self createAndLoadInterstitial];
}

- (GADInterstitial *) createAndLoadInterstitial {
    
    GADInterstitial *interstitial = [[GADInterstitial alloc] initWithAdUnitID: @"ca-app-pub-3940256099942544/4411468910"];
    [interstitial setDelegate: self];
    [interstitial loadRequest:[GADRequest request]];
    return interstitial;
    
}

//MARK:- User-defined methods

- (void) setStatusBarBackgroundColour {
    
    UIWindow *windowRoot = nil;
    NSArray *windows = [[UIApplication sharedApplication] windows];
    for (UIWindow *window in windows) {
        if (window.isKeyWindow) {
            windowRoot = window;
            break;
        }
    }
    
    UIView *statusBar = [[UIView alloc] initWithFrame: windowRoot.windowScene.statusBarManager.statusBarFrame];
    [statusBar setBackgroundColor: cStatusBarBackground];
    [self.view addSubview: statusBar];
    
}

- (void) setupLayout {
    
    [self setStatusBarBackgroundColour];
    [_gadBannerView setHidden: YES];
    activityIndicator = [Helpers createActivityIndicator];
    [self.view addSubview: activityIndicator];
    refreshControl = [Helpers createRefreshControlWithTintColor: nil Message: @"Fetching users..." Attributes: nil];
    [refreshControl addTarget: self action: @selector(fetchAllUsersFromFBFirestore) forControlEvents: UIControlEventValueChanged];
    [_tblChatListTableView setRefreshControl: refreshControl];
    [self->_noUsersLabel setHidden: YES];
    [self->_tblChatListTableView setHidden: NO];
    
    user = [[User allObjects] firstObject];
    
    UINib *chatListNib = [UINib nibWithNibName: kChatListCellIdentifier bundle: nil];
    [_tblChatListTableView registerNib: chatListNib forCellReuseIdentifier: kChatListCellIdentifier];
    
    [_gadBannerView setAdUnitID: kGoogleAdMobBannerAppUnitID];
    [_gadBannerView setRootViewController: self];
    [_gadBannerView setDelegate: self];
    [_gadBannerView loadRequest: [GADRequest request]];
    
    interstitialAdView = [self createAndLoadInterstitial];
    
}

- (void) fetchAllUsersFromFBFirestore {
    
    arrUsers = [[NSMutableArray alloc] init];
    
    [activityIndicator startAnimating];
    FIRCollectionReference *collectionRef = [[FIRFirestore firestore] collectionWithPath: kUsersCollection];
    [collectionRef getDocumentsWithCompletion:^(FIRQuerySnapshot * _Nullable snapshot, NSError * _Nullable error) {
       
        if (error) {
            [self.view makeToast: @"Error occured while retrieving users."];
        } else {
            
            for (FIRDocumentSnapshot *document in snapshot.documents) {
                
                if (![document.documentID isEqualToString: self->user.userID]) {
                    
                    NSMutableDictionary *dicUserData = [[NSMutableDictionary alloc] initWithDictionary: document.data];
                    User *userObj = [[User alloc] init];
                    [userObj setUserID: [dicUserData valueForKey: kUserIDField]];
                    [userObj setUserProfilePic: [dicUserData valueForKey: kUserProfilePicField]];
                    [userObj setUsername: [dicUserData valueForKey: kUsernameField]];
                    [userObj setEmail: [dicUserData valueForKey: kEmailField]];
                    [userObj setPassword: [dicUserData valueForKey: kPasswordField]];
                    [userObj setAboutMe: [dicUserData valueForKey: kAboutMeField]];
                    [self->arrUsers addObject: userObj];
                    
                }
                
            }
            
            if (self->arrUsers.count > 0) {
                [self->_noUsersLabel setHidden: YES];
                [self->_tblChatListTableView setDelegate: self];
                [self->_tblChatListTableView setDataSource: self];
                [self->_tblChatListTableView reloadData];
            } else {
                [self->_noUsersLabel setHidden: NO];
            }
            
        }
        [self->refreshControl endRefreshing];
        [self->activityIndicator stopAnimating];
        
    }];
    
}


@end
