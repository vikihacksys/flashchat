//
//  DisplayImageViewController.h
//  FlashChat
//
//  Created by Vivek Radadiya on 29/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DisplayImageViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *attachmentImageView;

- (IBAction)backButtonTapped:(UIButton *)sender;

@property NSString *attachmentImageURLString;

@end

NS_ASSUME_NONNULL_END
