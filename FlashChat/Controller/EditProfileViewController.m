//
//  EditProfileViewController.m
//  FlashChat
//
//  Created by Vivek Radadiya on 22/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "EditProfileViewController.h"
#import "PrefixHeader.pch"

@interface EditProfileViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    RLMRealm *realm;
    User *user;
    UIActivityIndicatorView *activityIndicator;
    UIImage *currentUserProfileImage;
    UIImagePickerController *imagePickerController;
    NSString *strUserProfileImageURL;
}
@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupLayout];
    
}


//MARK:- UIButton's action methods

- (IBAction)updateProfileButtonTapped:(UIButton *)sender {
    
    if ([self isProfileChanged]) {
        NSLog(@"");
        
        [activityIndicator startAnimating];
        if (_userProfileImageView.image != currentUserProfileImage) {
            [self uploadUserProfileImage];
        } else if (![_passwordTextField.text isEqualToString: user.password]) {
            [self updateCurrentUserPassword];
        } else {
            [self updateUserProfile];
        }
        
    }
    
}

- (IBAction)backButtonTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated: YES];
}


//MARK:- UIImagePickerController's delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info {
    
    UIImage *pickedImage = info[UIImagePickerControllerEditedImage];
    [_userProfileImageView setImage: pickedImage];
    [self dismissViewControllerAnimated: YES completion: nil];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated: YES completion: nil];
}


//MARK:- User-defined methods

- (void) setStatusBarBackgroundColour {
    
    UIWindow *windowRoot = nil;
    NSArray *windows = [[UIApplication sharedApplication] windows];
    for (UIWindow *window in windows) {
        if (window.isKeyWindow) {
            windowRoot = window;
            break;
        }
    }
    
    UIView *statusBar = [[UIView alloc] initWithFrame: windowRoot.windowScene.statusBarManager.statusBarFrame];
    [statusBar setBackgroundColor: cStatusBarBackground];
    [self.view addSubview: statusBar];
    
}

- (void) setupLayout {
    
    [self setStatusBarBackgroundColour];
    activityIndicator = [Helpers createActivityIndicator];
    [self.view addSubview: activityIndicator];
    [[_userProfileImageView layer] setCornerRadius: (_userProfileImageView.frame.size.height / 2.0)];
    [[_btnUpdateProfileButton layer] setCornerRadius: (_btnUpdateProfileButton.frame.size.height / 2.0)];
    realm = [RLMRealm defaultRealm];
    RLMResults<User *> *results = [User allObjects];
    user = [results firstObject];
    [_userProfileImageView setUserInteractionEnabled: YES];
    _userProfileImageView.sd_imageIndicator = SDWebImageActivityIndicator.grayLargeIndicator;
    [_userProfileImageView sd_setImageWithURL: [NSURL URLWithString: user.userProfilePic] placeholderImage: kUserProfilePlaceholderImage];
    strUserProfileImageURL = user.userProfilePic;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(userProfileImageViewTapped)];
    [_userProfileImageView addGestureRecognizer: tapGesture];
    currentUserProfileImage = _userProfileImageView.image;
    [_usernameTextField setText: user.username];
    [_emailTextField setText: user.email];
    [_emailTextField setEnabled: NO];
    [_passwordTextField setText: user.password];
    [_aboutMeTextField setText: user.aboutMe];
    
}

- (BOOL) isProfileChanged {
    
    if (_userProfileImageView.image != currentUserProfileImage) {
        return YES;
    } else if (![_usernameTextField.text isEqualToString: user.username]) {
        if ([[_usernameTextField text] isEqualToString: @""]) {
            [self.view makeToast: @"Username cannot be empty."];
            return NO;
        } else {
            return YES;
        }
    } else if (![_passwordTextField.text isEqualToString: user.password]) {
        if ([[_passwordTextField text] isEqualToString: @""]) {
            [self.view makeToast: @"Password cannot be empty."];
            return NO;
        } else {
            return YES;
        }
    } else if (![_aboutMeTextField.text isEqualToString: user.aboutMe]) {
        return YES;
    } else {
        return NO;
    }
    
}

- (void) userProfileImageViewTapped {
    
    imagePickerController = [[UIImagePickerController alloc] init];
    [imagePickerController setDelegate: self];
    [imagePickerController setAllowsEditing: YES];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle: @"Select an image" message: @"" preferredStyle: UIAlertControllerStyleActionSheet];
    
    UIAlertAction *galleryAction = [UIAlertAction actionWithTitle: @"From Gallery" style: UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self->imagePickerController setSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
        [self presentViewController: self->imagePickerController animated: YES completion: nil];
        
    }];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle: @"From Camera" style: UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self->imagePickerController setSourceType: UIImagePickerControllerSourceTypeCamera];
        [self presentViewController: self->imagePickerController animated: YES completion: nil];
        
    }];
    
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle: @"Delete image" style: UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self->_userProfileImageView setImage: kUserProfilePlaceholderImage];
        
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle: @"Cancel" style: UIAlertActionStyleCancel handler: nil];
    
    [alertController addAction: galleryAction];
    [alertController addAction: cameraAction];
    [alertController addAction: deleteAction];
    [alertController addAction: cancelAction];
    
    [self presentViewController: alertController animated: YES completion: nil];
    
}

- (void) uploadUserProfileImage {
    
    NSString *userPath = [[[_emailTextField text] componentsSeparatedByString: @"@"] firstObject];
    FIRStorageReference *profileRef = [[FirebaseManager userProfileImageRef] child: [NSString stringWithFormat: @"%@/%@", userPath, kFBStorageProfileName]];
    FIRStorageMetadata *metadata = [[FIRStorageMetadata alloc] init];
    metadata.contentType = @"image/jpeg";
    
    [profileRef putData: UIImageJPEGRepresentation([_userProfileImageView image], 1.0) metadata: metadata completion:^(FIRStorageMetadata * _Nullable metadata, NSError * _Nullable error) {
        if (error) {
            [self.view makeToast: @"Error updating profile."];
            [self->activityIndicator stopAnimating];
        } else {
            [profileRef downloadURLWithCompletion:^(NSURL * _Nullable URL, NSError * _Nullable error) {
                if (error) {
                    [self.view makeToast: @"Error updating profile."];
                    [self->activityIndicator stopAnimating];
                } else {
                    self->strUserProfileImageURL = [URL absoluteString];
                    [self updateCurrentUserPassword];
                }
            }];
        }
    }];
    
}

- (void) updateCurrentUserPassword {
    
    if (![_passwordTextField.text isEqualToString: user.password]) {
        
        FIRUser *currentUser = [[FIRAuth auth] currentUser];
        [currentUser updatePassword: [_passwordTextField text] completion:^(NSError * _Nullable error) {
            if (error) {
                [self.view makeToast: @"Error updating profile."];
                [self->activityIndicator stopAnimating];
            } else {
                [[FIRAuth auth] updateCurrentUser: currentUser completion:^(NSError * _Nullable error) {
                    if (error) {
                        [self.view makeToast: @"Error updating profile."];
                        [self->activityIndicator stopAnimating];
                    } else {
                        [self updateUserProfile];
                    }
                }];
            }
        }];
        
    } else {
        [self updateUserProfile];
    }
    
}

- (void) updateUserProfile {
    
    NSString *strUserID = user.userID;
    NSString *strProfileURL = strUserProfileImageURL;
    NSString *strUsername = self->_usernameTextField.text;
    NSString *strEmail = self->_emailTextField.text;
    NSString *strPassword = self->_passwordTextField.text;
    NSString *strAboutMe = self->_aboutMeTextField.text;
    
    [[[[FIRFirestore firestore] collectionWithPath: kUsersCollection] documentWithPath: user.userID] updateData: @{
        
        kUserIDField: strUserID,
        kUserProfilePicField: strProfileURL,
        kUsernameField: strUsername,
        kEmailField: strEmail,
        kPasswordField: strPassword,
        kAboutMeField: strAboutMe
        
    } completion:^(NSError * _Nullable error) {
        
        if (error) {
            [self.view makeToast: @"Error updating profile."];
        } else {
            
            NSError *error;
            RLMResults<User *> *user = [User allObjects];
            [[RLMRealm defaultRealm] transactionWithBlock:^{
                
                [[user firstObject] setValue: strProfileURL forKey: kUserProfilePicField];
                [[user firstObject] setValue: strUsername forKey: kUsernameField];
                [[user firstObject] setValue: strEmail forKey: kEmailField];
                [[user firstObject] setValue: strPassword forKey: kPasswordField];
                [[user firstObject] setValue: strAboutMe forKey: kAboutMeField];
                [[user firstObject] setValue: @YES forKey: kIsLoggedInField];
                
            } error: &error];
            
            if (error) {
                [self.view makeToast: @"Error updating profile."];
            } else {
                [self.view makeToast: @"Profile updated successfully."];
            }
            [self->activityIndicator stopAnimating];
            
        }
        [self->activityIndicator stopAnimating];
        
    }];
    
}

@end
