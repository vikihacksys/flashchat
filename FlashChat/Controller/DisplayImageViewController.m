//
//  DisplayImageViewController.m
//  FlashChat
//
//  Created by Vivek Radadiya on 29/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "DisplayImageViewController.h"
#import "PrefixHeader.pch"

@interface DisplayImageViewController ()

@end

@implementation DisplayImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupLayout];
    
}

- (IBAction)backButtonTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated: YES];
}


//MARK:- User-defined methods

- (void) setStatusBarBackgroundColour {
    
    UIWindow *windowRoot = nil;
    NSArray *windows = [[UIApplication sharedApplication] windows];
    for (UIWindow *window in windows) {
        if (window.isKeyWindow) {
            windowRoot = window;
            break;
        }
    }
    
    UIView *statusBar = [[UIView alloc] initWithFrame: windowRoot.windowScene.statusBarManager.statusBarFrame];
    [statusBar setBackgroundColor: cStatusBarBackground];
    [self.view addSubview: statusBar];
    
}

- (void) setupLayout {
    
    [self setStatusBarBackgroundColour];
    _attachmentImageView.sd_imageIndicator = SDWebImageActivityIndicator.grayLargeIndicator;
    [_attachmentImageView sd_setImageWithURL: [NSURL URLWithString: _attachmentImageURLString] placeholderImage: nil];
    
}

@end
