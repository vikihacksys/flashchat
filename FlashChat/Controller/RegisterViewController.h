//
//  RegisterViewController.h
//  FlashChat
//
//  Created by Vivek Radadiya on 18/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RegisterViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserProfileImageView;
@property (weak, nonatomic) IBOutlet UILabel *registerTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextView *aboutMeTextView;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;


- (IBAction)backButtonTapped:(UIButton *)sender;
- (IBAction)registerButtonTapped:(UIButton *)sender;
- (IBAction)userProfileButtonTapped:(UIButton *)sender;


@end

NS_ASSUME_NONNULL_END
