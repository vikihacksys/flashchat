//
//  ProfileViewController.h
//  FlashChat
//
//  Created by Vivek Radadiya on 22/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <UIKit/UIKit.h>
@class User;

NS_ASSUME_NONNULL_BEGIN

@interface ProfileViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *userProfileImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *aboutMeLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnProfileEditButton;
@property (weak, nonatomic) IBOutlet UIButton *btnLogoutButton;
@property (weak, nonatomic) IBOutlet UIButton *btnBackButton;

- (IBAction)backButtonTapped:(UIButton *)sender;
- (IBAction)profileButtonTapped:(UIButton *)sender;
- (IBAction)logoutButtonTapped:(UIButton *)sender;

@property User *receiverUser;

@end

NS_ASSUME_NONNULL_END
