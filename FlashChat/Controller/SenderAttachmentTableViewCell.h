//
//  SenderAttachmentTableViewCell.h
//  FlashChat
//
//  Created by Vivek Radadiya on 28/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SenderAttachmentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *attachmentContentView;
@property (weak, nonatomic) IBOutlet UIImageView *attachmentImageView;
@property (weak, nonatomic) IBOutlet UILabel *attachmentTimeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeadingForAttachmentContentView;

@end

NS_ASSUME_NONNULL_END
