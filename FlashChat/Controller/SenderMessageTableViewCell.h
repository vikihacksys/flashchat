//
//  SenderMessageTableViewCell.h
//  FlashChat
//
//  Created by Vivek Radadiya on 27/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SenderMessageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *messageContentView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageTimeLabel;


@end

NS_ASSUME_NONNULL_END
