//
//  TabBarViewController.h
//  FlashChat
//
//  Created by Vivek Radadiya on 22/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TabBarViewController : UITabBarController

@end

NS_ASSUME_NONNULL_END
