//
//  ViewController.m
//  FlashChat
//
//  Created by Vivek Radadiya on 18/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "ViewController.h"
#import "PrefixHeader.pch"

@interface ViewController ()
{
    
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupLayout];
    [self animateAppName];
    
}


- (IBAction)registerButtonTapped:(UIButton *)sender {
    RegisterViewController *registerVC = [self.storyboard instantiateViewControllerWithIdentifier: @"RegisterViewController"];
    [self.navigationController pushViewController: registerVC animated: YES];
}

- (IBAction)loginButtonTapped:(UIButton *)sender {
    LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier: @"LoginViewController"];
    [self.navigationController pushViewController: loginVC animated: YES];
}


//MARK:- User-defined methods

- (void) animateAppName {
    
    for (int i = 0; i < [kAppName length]; i++) {
        
        [NSTimer scheduledTimerWithTimeInterval: (0.1 * i) repeats: NO block:^(NSTimer * _Nonnull timer) {
            [self->_lblAppNameLabel setText: [NSString stringWithFormat: @"%@%@", [self->_lblAppNameLabel text], [kAppName substringWithRange: NSMakeRange(i, 1)]]];
        }];
        
    }
    
}

- (void) setupLayout {
    
    [_lblAppNameLabel setText: @""];
    [[_btnLogin layer] setCornerRadius: (_btnLogin.frame.size.height / 2.0)];
    [[_btnRegister layer] setCornerRadius: (_btnRegister.frame.size.height / 2.0)];
    
}

@end
