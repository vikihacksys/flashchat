//
//  ViewController.h
//  FlashChat
//
//  Created by Vivek Radadiya on 18/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


@property (weak, nonatomic) IBOutlet UILabel *lblAppNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopForAppNameLabel;

- (IBAction)loginButtonTapped:(UIButton *)sender;
- (IBAction)registerButtonTapped:(UIButton *)sender;


@end

