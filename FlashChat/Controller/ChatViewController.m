//
//  ChatViewController.m
//  FlashChat
//
//  Created by Vivek Radadiya on 27/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "ChatViewController.h"
#import "PrefixHeader.pch"

@interface ChatViewController () <UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    RLMResults<User *> *results;
    User *user;
    NSMutableArray *arrMessages;
    NSMutableDictionary *dicMessages;
    NSString *strMessageType;
    FIRCollectionReference *chatsRef, *senderCollectionRef;
    FIRDocumentReference *senderRef, *receiverRef;
    UIImagePickerController *imagePickerController;
    UIActivityIndicatorView *activityIndicator;
}
@end

@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupLayout];
    
}

- (void) viewWillAppear: (BOOL) animated {
    [super viewWillAppear: animated];
    
    [_receiverUsernameLabel setText: _receiverUser.username];
    
}

//MARK:- UIButton's action methods

- (IBAction)sendMessageButtonTapped:(UIButton *)sender {
    
    NSString *strMessage = _messageTextView.text;
    if (![strMessage isEqualToString: @""]) {
        [_messageTextView setText: @""];
        [self sendMessage: strMessage withAttachment: @"" messageType: kMessageTypeText];
    }
    
}

- (IBAction)attachmentButtonTapped:(UIButton *)sender {
    if (_attachmentView.isHidden) {
        [_attachmentView setHidden: NO];
    } else {
        [_attachmentView setHidden: YES];
    }
}

- (IBAction)shareImageButtonTapped:(UIButton *)sender {
    [_attachmentView setHidden: YES];
    [self openImagePickerController];
}

- (IBAction)shareLocationButtonTapped:(UIButton *)sender {
    [_attachmentView setHidden: YES];
}

- (IBAction)profileButtonTapped:(UIButton *)sender {
    ProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier: kProfileVC];
    profileVC.receiverUser = _receiverUser;
    [self.navigationController pushViewController: profileVC animated: YES];
}

- (IBAction)backButtonTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated: YES];
}


//MARK:- UIImagePickerController's methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info {
    
    UIImage *pickedImage = info[UIImagePickerControllerEditedImage];
    NSData *imageData = UIImageJPEGRepresentation(pickedImage, 1.0);
    [self.navigationController dismissViewControllerAnimated: YES completion:^{
        [self uploadImageToFBFirestore: imageData];
    }];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
}

- (void) openImagePickerController {
    
    imagePickerController = [[UIImagePickerController alloc] init];
    [imagePickerController setDelegate: self];
    [imagePickerController setAllowsEditing: YES];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle: @"Select an image" message: @"" preferredStyle: UIAlertControllerStyleActionSheet];
    
    UIAlertAction *galleryAction = [UIAlertAction actionWithTitle: @"From Gallery" style: UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self->imagePickerController setSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
        [self presentViewController: self->imagePickerController animated: YES completion: nil];
        
    }];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle: @"From Camera" style: UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self->imagePickerController setSourceType: UIImagePickerControllerSourceTypeCamera];
        [self presentViewController: self->imagePickerController animated: YES completion: nil];
        
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle: @"Cancel" style: UIAlertActionStyleCancel handler: nil];
    
    [alertController addAction: galleryAction];
    [alertController addAction: cameraAction];
    [alertController addAction: cancelAction];
    
    [self presentViewController: alertController animated: YES completion: nil];
    
}

- (void) uploadImageToFBFirestore: (NSData *) imageData {
    
    FIRStorageReference *profileRef = [[FirebaseManager chatImageRef] child: user.userID];
    FIRStorageMetadata *metadata = [[FIRStorageMetadata alloc] init];
    metadata.contentType = @"image/jpeg";
    
    [profileRef putData: imageData metadata: metadata completion:^(FIRStorageMetadata * _Nullable metadata, NSError * _Nullable error) {
        
        if (error) {
            [self.view makeToast: @"Error sending message."];
        } else {
            
            [profileRef downloadURLWithCompletion:^(NSURL * _Nullable URL, NSError * _Nullable error) {
                
                if (error) {
                    [self.view makeToast: @"Error sending message."];
                } else {
                    [self sendMessage: @"" withAttachment: [URL absoluteString] messageType: kMessageTypeImage];
                }
                
            }];
            
        }
        
    }];
    
}


//MARK:- UITableView's delegate and datasource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arrMessages.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSDictionary *dic = [[NSDictionary alloc] initWithDictionary: [arrMessages objectAtIndex: section]];
    NSArray *messages = [[NSArray alloc] initWithArray: [[dic allValues] firstObject]];
    return messages.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSDictionary *dic = [[NSDictionary alloc] initWithDictionary: [arrMessages objectAtIndex: section]];
    NSString *strDate = [[dic allKeys] firstObject];
    NSArray *dateArray = [[NSArray alloc] initWithArray: [strDate componentsSeparatedByString: @" "]];
    NSString *strHeader = [[NSString alloc] initWithFormat: @"%@, %@", [dateArray firstObject],[[self extractTimeAndDayStringFromDateString: strDate] lastObject]];
    
    UIView *headerView = [[UIView alloc] initWithFrame: CGRectMake(0.0, 0.0, tableView.frame.size.width, 21.0)];
    [headerView setBackgroundColor: UIColor.clearColor];
    [headerView setAutoresizingMask: UIViewAutoresizingNone];
    
    UILabel *dateLabel = [[UILabel alloc] initWithFrame: CGRectMake(0.0, 0.0, headerView.frame.size.width, headerView.frame.size.height)];
    [dateLabel setText: strHeader];
    [dateLabel setFont: [UIFont fontWithName: @"HelveticaNeue" size: 13]];
    [dateLabel setTextColor: cStatusBarBackground];
    [dateLabel setTextAlignment: NSTextAlignmentCenter];
    [dateLabel setBackgroundColor: UIColor.whiteColor];
    [headerView addSubview: dateLabel];
    
    return headerView;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 21.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dic = [[NSDictionary alloc] initWithDictionary: [arrMessages objectAtIndex: indexPath.section]];
    NSArray *messages = [[NSArray alloc] initWithArray: [[dic allValues] firstObject]];
    NSDictionary *dicMessage = [[NSDictionary alloc] initWithDictionary: [messages objectAtIndex: indexPath.row]];
    NSString *strMessageType = [dicMessage valueForKey: kMessageTypeField];
    
    if ([strMessageType isEqualToString: kMessageTypeText]) {
        return UITableViewAutomaticDimension;
    } else {
        CGFloat leadingSpace = ([UIScreen mainScreen].bounds.size.width / 2.0);
        return leadingSpace;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dic = [[NSDictionary alloc] initWithDictionary: [arrMessages objectAtIndex: indexPath.section]];
    NSArray *messages = [[NSArray alloc] initWithArray: [[dic allValues] firstObject]];
    NSDictionary *dicMessage = [[NSDictionary alloc] initWithDictionary: [messages objectAtIndex: indexPath.row]];
    
    NSString *strMessageType = [dicMessage valueForKey: kMessageTypeField];
    NSString *strMessage = [dicMessage valueForKey: kMessageField];
    NSString *strMessageAttachment = [dicMessage valueForKey: kMessageAttachmentField];
    NSString *strDateTime = [dicMessage valueForKey: kDateTimeField];
    
    if ([[dicMessage valueForKey: kSenderIDField] isEqualToString: user.userID]) {
        
        if ([strMessageType isEqualToString: kMessageTypeText]) {
            
            SenderMessageTableViewCell *senderMessageCell = (SenderMessageTableViewCell *) [tableView dequeueReusableCellWithIdentifier: kSenderMessageCellIdentifier forIndexPath: indexPath];
            
            [[senderMessageCell.messageContentView layer] setCornerRadius: 8.0];
            [senderMessageCell.messageTimeLabel setText: [[self extractTimeAndDayStringFromDateString: strDateTime] firstObject]];
            [senderMessageCell.messageLabel setText: strMessage];
            
            return senderMessageCell;
            
        } else {
            
            SenderAttachmentTableViewCell *senderAttachmentCell = (SenderAttachmentTableViewCell *) [tableView dequeueReusableCellWithIdentifier: kSenderAttachmentCellIdentifier forIndexPath: indexPath];
            
            CGFloat leadingSpace = ([UIScreen mainScreen].bounds.size.width / 2.0);
            [senderAttachmentCell.constraintLeadingForAttachmentContentView setConstant: leadingSpace];
            [[senderAttachmentCell.attachmentContentView layer] setCornerRadius: 8.0];
            [[senderAttachmentCell.attachmentImageView layer] setCornerRadius: 8.0];
            senderAttachmentCell.attachmentImageView.sd_imageIndicator = SDWebImageActivityIndicator.grayLargeIndicator;
            [senderAttachmentCell.attachmentImageView sd_setImageWithURL: [NSURL URLWithString: strMessageAttachment] placeholderImage: kUserProfilePlaceholderImage];
//            [senderAttachmentCell.attachmentImageView performSelector: @selector(displayImageAttachment:) withObject: strMessageAttachment];
            [senderAttachmentCell.attachmentTimeLabel setText: [[self extractTimeAndDayStringFromDateString: strDateTime] firstObject]];
            
            return senderAttachmentCell;
            
        }
        
    } else {
        
        if ([strMessageType isEqualToString: kMessageTypeText]) {
            
            ReceiverMessageTableViewCell *receiverMessageCell = (ReceiverMessageTableViewCell *) [tableView dequeueReusableCellWithIdentifier: kReceiverMessageCellIdentifier forIndexPath: indexPath];
            
            [[receiverMessageCell.messageContentView layer] setCornerRadius: 8.0];
            [receiverMessageCell.messageTimeLabel setText: [[self extractTimeAndDayStringFromDateString: strDateTime] firstObject]];
            [receiverMessageCell.messageLabel setText: strMessage];
            
            return receiverMessageCell;
            
        } else {
            
            ReceiverAttachmentTableViewCell *receiverAttachmentCell = (ReceiverAttachmentTableViewCell *) [tableView dequeueReusableCellWithIdentifier: kReceiverAttachmentCellIdentifier forIndexPath: indexPath];
            
            CGFloat leadingSpace = ([UIScreen mainScreen].bounds.size.width / 2.0);
            [receiverAttachmentCell.constraintTrailingForAttachmentContentView setConstant: leadingSpace];
            [[receiverAttachmentCell.attachmentContentView layer] setCornerRadius: 8.0];
            [[receiverAttachmentCell.attachmentImageView layer] setCornerRadius: 8.0];
            receiverAttachmentCell.attachmentImageView.sd_imageIndicator = SDWebImageActivityIndicator.grayLargeIndicator;
            [receiverAttachmentCell.attachmentImageView sd_setImageWithURL: [NSURL URLWithString: strMessageAttachment] placeholderImage: kUserProfilePlaceholderImage];
            [receiverAttachmentCell.attachmentTimeLabel setText: [[self extractTimeAndDayStringFromDateString: strDateTime] firstObject]];
            
            return receiverAttachmentCell;
            
        }
        
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dic = [[NSDictionary alloc] initWithDictionary: [arrMessages objectAtIndex: indexPath.section]];
    NSArray *messages = [[NSArray alloc] initWithArray: [[dic allValues] firstObject]];
    NSDictionary *dicMessage = [[NSDictionary alloc] initWithDictionary: [messages objectAtIndex: indexPath.row]];
    NSString *strMessageType = [dicMessage valueForKey: kMessageTypeField];

    if ([strMessageType isEqualToString: kMessageTypeImage]) {
        DisplayImageViewController *displayImageVC = [self.storyboard instantiateViewControllerWithIdentifier: kDisplayImageVC];
        displayImageVC.attachmentImageURLString = [dicMessage valueForKey: kMessageAttachmentField];
        [self.navigationController pushViewController: displayImageVC animated: YES];
    }

    if ([strMessageType isEqualToString: kMessageTypeLocation]) {

    }
    
}

- (void) displayImageAttachment: (NSString *) image {
    DisplayImageViewController *displayImageVC = [self.storyboard instantiateViewControllerWithIdentifier: kDisplayImageVC];
    displayImageVC.attachmentImageURLString = image;
    [self.navigationController pushViewController: displayImageVC animated: YES];
}

- (void) displayLocationAttachment {
    
}


//MARK:- User-defined methods

- (void) setStatusBarBackgroundColour {
    
    UIWindow *windowRoot = nil;
    NSArray *windows = [[UIApplication sharedApplication] windows];
    for (UIWindow *window in windows) {
        if (window.isKeyWindow) {
            windowRoot = window;
            break;
        }
    }
    
    UIView *statusBar = [[UIView alloc] initWithFrame: windowRoot.windowScene.statusBarManager.statusBarFrame];
    [statusBar setBackgroundColor: cStatusBarBackground];
    [self.view addSubview: statusBar];
    
}

- (void) setupLayout {
    
    [self setStatusBarBackgroundColour];
    [_noChatsAvailableLabel setHidden: YES];
    [_attachmentView setHidden: YES];
    [[_btnShareImageButton layer] setCornerRadius: (_btnShareImageButton.frame.size.height / 2.0)];
    [[_btnShareLocationButton layer] setCornerRadius: (_btnShareLocationButton.frame.size.height / 2.0)];
    [_messageTextView setText: @""];
    [[_messageTextView layer] setCornerRadius: 4.0];
    if ([UIScreen mainScreen].bounds.size.height >= 812) {
        [_constraintHeightForBottomView setConstant: 88.0];
    } else {
        [_constraintHeightForBottomView setConstant: 44.0];
    }
    
    activityIndicator = [Helpers createActivityIndicator];
    [self.view addSubview: activityIndicator];
    
    UINib *senderMessageNib = [UINib nibWithNibName: kSenderMessageCellIdentifier bundle: nil];
    UINib *receiverMessageNib = [UINib nibWithNibName: kReceiverMessageCellIdentifier bundle: nil];
    UINib *senderAttachmentNib = [UINib nibWithNibName: kSenderAttachmentCellIdentifier bundle: nil];
    UINib *receiverAttachmentNib = [UINib nibWithNibName: kReceiverAttachmentCellIdentifier bundle: nil];
    [_messageTableView registerNib: senderMessageNib forCellReuseIdentifier: kSenderMessageCellIdentifier];
    [_messageTableView registerNib: receiverMessageNib forCellReuseIdentifier: kReceiverMessageCellIdentifier];
    [_messageTableView registerNib: senderAttachmentNib forCellReuseIdentifier: kSenderAttachmentCellIdentifier];
    [_messageTableView registerNib: receiverAttachmentNib forCellReuseIdentifier: kReceiverAttachmentCellIdentifier];
    
    user = [[User allObjects] firstObject];
    
    chatsRef = [[FIRFirestore firestore] collectionWithPath: kChatsCollection];
    senderCollectionRef = [[chatsRef documentWithPath: user.userID] collectionWithPath: _receiverUser.userID];
    
    __weak typeof(self) weakSelf = self;
    [senderCollectionRef addSnapshotListenerWithIncludeMetadataChanges: NO listener:^(FIRQuerySnapshot * _Nullable snapshot, NSError * _Nullable error) {
       
        if (error) {
            NSLog(@"Error listening changes: %@", error.localizedDescription);
        } else {
            
            self->dicMessages = [[NSMutableDictionary alloc] init];
            
            __strong typeof(self) strongSelf = weakSelf;
            if (strongSelf) {
                
                for (FIRDocumentSnapshot *document in snapshot.documents) {
                    
                    NSDictionary *dicDocument = [document data];
                    NSString *strDate = [NSString stringWithFormat: @"%@", [Helpers dateFromString: [dicDocument valueForKey: kDateTimeField]]];
                    
                    NSMutableArray *messages;
                    if ([[strongSelf->dicMessages allKeys] containsObject: strDate]) {
                        messages = [[NSMutableArray alloc] initWithArray: [strongSelf->dicMessages valueForKey: strDate]];
                    } else {
                        messages = [[NSMutableArray alloc] init];
                    }
                    [messages addObject: dicDocument];
                    [strongSelf->dicMessages setValue: messages forKey: strDate];
                    
                }
                
                NSArray *keys = [strongSelf->dicMessages allKeys];
                NSArray *sortedKeys = [keys sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                    return [a compare:b];
                }];
                
                self->arrMessages = [[NSMutableArray alloc] init];
                for (NSString *key in sortedKeys) {
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                    NSArray *arr = [[NSArray alloc] initWithArray: [strongSelf->dicMessages valueForKey: key]];
                    NSArray *sortedArray = [arr sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                        NSString *first = [a valueForKey: kDateTimeField];
                        NSString *second = [b valueForKey: kDateTimeField];
                        return [first compare:second];
                    }];
                    [dic setValue: sortedArray forKey: key];
                    [strongSelf->arrMessages addObject: dic];
                }
                
                if (strongSelf->arrMessages.count > 0) {
                    [strongSelf->_noChatsAvailableLabel setHidden: YES];
                    NSDictionary *dic = [[NSDictionary alloc] initWithDictionary: [strongSelf->arrMessages lastObject]];
                    NSArray *messages = [[NSArray alloc] initWithArray: [[dic allValues] firstObject]];
                    [strongSelf->_messageTableView setDelegate: strongSelf];
                    [strongSelf->_messageTableView setDataSource: strongSelf];
                    [strongSelf->_messageTableView reloadData];
                    [strongSelf->_messageTableView scrollToRowAtIndexPath: [NSIndexPath indexPathForRow: (messages.count - 1) inSection: (strongSelf->arrMessages.count - 1)] atScrollPosition: UITableViewScrollPositionBottom animated: YES];
                } else {
                    [strongSelf->_noChatsAvailableLabel setHidden: NO];
                }
                
            } else {
                NSLog(@"Self doesn't exists.");
            }
            
        }
        
    }];
    
}

- (void) sendMessage: (NSString *) message withAttachment: (NSString *) attachment messageType: (NSString *) type {
    
    NSString *strDate = [NSString stringWithFormat: @"%@", [NSDate date]];
    NSString *messageID = [chatsRef documentWithAutoID].documentID;
    receiverRef = [[[chatsRef documentWithPath: _receiverUser.userID] collectionWithPath: user.userID] documentWithPath: messageID];
    senderRef = [senderCollectionRef documentWithPath: messageID];
    
    NSDictionary *dicMessage = @{
        
        kMessageIDField: messageID,
        kMessageField: message,
        kMessageAttachmentField: attachment,
        kMessageTypeField: type,
        kDateTimeField: strDate,
        kSenderIDField: user.userID,
        kReceiverIDField: _receiverUser.userID
        
    };

    [senderRef setData: dicMessage completion:^(NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"Error sending message.");
        } else {
            
        }
        
    }];
    
    [receiverRef setData: dicMessage completion:^(NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"Error sending message.");
        } else {
            
        }
        
    }];
    
}

- (NSArray *) extractTimeAndDayStringFromDateString: (NSString *) date {
    
    NSArray *dateArray = [[NSArray alloc] initWithArray: [date componentsSeparatedByString: @" "]];
    NSArray *timeArray = [[NSArray alloc] initWithArray: [[dateArray objectAtIndex: 1] componentsSeparatedByString: @":"]];
    NSString *strTime = [NSString stringWithFormat: @"%@:%@", [timeArray objectAtIndex: 0], [timeArray objectAtIndex: 1]];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components: NSCalendarUnitDay | NSCalendarUnitWeekday fromDate: [Helpers dateFromString: date]];
    NSString *dayName = [calendar.weekdaySymbols objectAtIndex: (dateComponents.weekday - 1)];
    
    return @[strTime, dayName];
    
}

@end
