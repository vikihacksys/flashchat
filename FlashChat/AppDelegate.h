//
//  AppDelegate.h
//  FlashChat
//
//  Created by Vivek Radadiya on 18/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrefixHeader.pch"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@end

