//
//  SceneDelegate.m
//  FlashChat
//
//  Created by Vivek Radadiya on 18/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "SceneDelegate.h"
#import "PrefixHeader.pch"

@interface SceneDelegate ()
{
    User *user;
    UIStoryboard *storyBoard;
    RLMRealm *realm;
}
@end

@implementation SceneDelegate


- (void)scene:(UIScene *)scene willConnectToSession:(UISceneSession *)session options:(UISceneConnectionOptions *)connectionOptions {
    // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
    // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
    // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
    
    //    NSError *error;
    //    NSData *userData = [[NSUserDefaults standardUserDefaults] objectForKey: kUser];
    //    user = [NSKeyedUnarchiver unarchivedObjectOfClass: [User class] fromData: userData error: &error];
    
    [[NSNotificationCenter defaultCenter] removeObserver: self name: kSetupRootVCNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(setupRootViewController) name: kSetupRootVCNotification object: nil];
    
    [self setupRootViewController];
    
}


- (void)sceneDidDisconnect:(UIScene *)scene {
    // Called as the scene is being released by the system.
    // This occurs shortly after the scene enters the background, or when its session is discarded.
    // Release any resources associated with this scene that can be re-created the next time the scene connects.
    // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
}


- (void)sceneDidBecomeActive:(UIScene *)scene {
    // Called when the scene has moved from an inactive state to an active state.
    // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
}


- (void)sceneWillResignActive:(UIScene *)scene {
    // Called when the scene will move from an active state to an inactive state.
    // This may occur due to temporary interruptions (ex. an incoming phone call).
}


- (void)sceneWillEnterForeground:(UIScene *)scene {
    // Called as the scene transitions from the background to the foreground.
    // Use this method to undo the changes made on entering the background.
}


- (void)sceneDidEnterBackground:(UIScene *)scene {
    // Called as the scene transitions from the foreground to the background.
    // Use this method to save data, release shared resources, and store enough scene-specific state information
    // to restore the scene back to its current state.
}


//MARK:- User-defined methods

- (void) setupRootViewController {
    
    RLMResults<User *> *results = [User allObjects];

    storyBoard = [UIStoryboard storyboardWithName: kStoryBoardName bundle: nil];
    
    if ([results firstObject].isLoggedIn) {
        
        TabBarViewController *tabBarVC = [storyBoard instantiateViewControllerWithIdentifier: kTabBarVC];
        [self.window setRootViewController: tabBarVC];
        [self.window makeKeyAndVisible];
        
    } else {
        
        ViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier: kBaseVC];
        UINavigationController *navigationVC = [[UINavigationController alloc] initWithRootViewController: viewController];
        [navigationVC setNavigationBarHidden: YES];
        [self.window setRootViewController: navigationVC];
        [self.window makeKeyAndVisible];
        
    }
    
}



@end
