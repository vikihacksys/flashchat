//
//  AppDelegate.m
//  FlashChat
//
//  Created by Vivek Radadiya on 18/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "AppDelegate.h"
#import "PrefixHeader.pch"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [FIRApp configure];
    [[IQKeyboardManager sharedManager] setEnable: YES];
    [[GADMobileAds sharedInstance] startWithCompletionHandler: nil];
    
    
    //Check this for more details: https://realm.io/docs/objc/latest/#migrations
    
    RLMRealmConfiguration *realmConfig = [RLMRealmConfiguration defaultConfiguration];
    [realmConfig setSchemaVersion: 1];
    [realmConfig setMigrationBlock:^(RLMMigration * _Nonnull migration, uint64_t oldSchemaVersion) {

        if (oldSchemaVersion < 1) {

        }

    }];

    [RLMRealmConfiguration setDefaultConfiguration: realmConfig];

//    NSLog(@"Realm path : %@", [[[RLMRealm defaultRealm] configuration] fileURL]);
    
    return YES;
}


#pragma mark - UISceneSession lifecycle


- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}


@end
