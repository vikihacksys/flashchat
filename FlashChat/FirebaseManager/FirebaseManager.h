//
//  FirebaseManager.h
//  FlashChat
//
//  Created by Vivek Radadiya on 26/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Firebase/Firebase.h>

NS_ASSUME_NONNULL_BEGIN

@interface FirebaseManager : NSObject

+ (FIRStorageReference *) rootFBStorageRef;

+ (FIRStorageReference *) userProfileImageRef;

+ (FIRStorageReference *) chatImageRef;

@end

NS_ASSUME_NONNULL_END
