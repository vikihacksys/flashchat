//
//  FirebaseManager.m
//  FlashChat
//
//  Created by Vivek Radadiya on 26/08/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "FirebaseManager.h"
#import "PrefixHeader.pch"

@implementation FirebaseManager

+ (FIRStorageReference *) rootFBStorageRef {
    return [[FIRStorage storage] reference];
}

+ (FIRStorageReference *) userProfileImageRef {
    return [[self rootFBStorageRef] child: kFBStorageImageRefPathName];
}

+ (FIRStorageReference *) chatImageRef {
    return [[self rootFBStorageRef] child: kFBStorageChatRefPathName];
}

@end
