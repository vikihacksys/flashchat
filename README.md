# FlashChat

> An iOS chat application that allow users to create an account and to chat between other registered users in the application. 

## Screenshots
![Home Screen](FlashChat/Resources/HomeScreen.png)
![Users Screen](FlashChat/Resources/ChatListScreen.png)
![Chat Screen](FlashChat/Resources/ChatScreen.png)
![Profile Screen](FlashChat/Resources/ProfileScreen.png)

## Technologies
* Objective - C
* Firebase - Auth, FireStore and Storage
* Realm Database
* Google AdMobs

## Features
* Delegate Pattern
* MVC Design Pattern

## Contact
Created by [@vikihacksys](https://gitlab.com/vikihacksys) - feel free to contact me!
